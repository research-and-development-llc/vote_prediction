# vote_prediction

### David Dewhurst

A library of tools to predict voting outcomes at the micro and macro levels. 
Currently consists of 

+  **`semantic_analysis`**: Analyzes survey text using Latent Dirichlet 
Allocation, VADER sentiment analysis, and finding ngrams
+  **`rcsimple`**: wrappers for sklearn functions for easy-to-use regression

Documentation available at [the project website](https://research-and-development-llc.gitlab.io/vote_prediction/). 