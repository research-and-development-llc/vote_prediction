"""! semantic_analysis
David Dewhurst
Research and Development, LLC
Copyright 2017-, all rights reserved to the author
DO NOT DISTRIBUTE

Analyzes sentiment, finds ngrams, and reports on topics occurring in text.
"""

from __future__ import print_function
import csv
import sys
import warnings

with warnings.catch_warnings():
    warnings.filterwarnings("ignore")

    import numpy as np
    from time import time
    from sklearn.feature_extraction.text import CountVectorizer
    from sklearn.decomposition import LatentDirichletAllocation
    from sklearn.feature_extraction.text import ENGLISH_STOP_WORDS
    import nltk
    from nltk.sentiment import vader
    from nltk.collocations import *
    import matplotlib.pyplot as plt
    import scipy.stats as stats


def csv2array(filename):
    """! Tries to convert a csv file to a numpy array.

    @param filename (str): Absolute path to a csv file

    @return (numpy array): numpy array of the csv
    """
    with open(filename, 'r') as f:
        raw = []
	reader = csv.reader(f)
	for line in reader:
            raw.append(line)

	return np.asarray(raw)


def lda(text, n_features=1000, n_topics=4, n_top_words=4):
    """! Perform latent dirichlet allocation for words

    @param text (iterable): an iterable of strings
    @param n_features (int): number of features for vectorization, default
    is 1000
    @param n_topics (int): number of topics in the documents. Default is 4.
    @param n_top_words (int): number of words per topic. Default is 4.

    @return tops (list): a list of strings with n_topics elements.
    """
    # add to stop words 
    # the word inapplicable is a result of the questionnaire
    stop_words = ENGLISH_STOP_WORDS.union(['inapplicable'])

    tf_vectorizer = CountVectorizer(max_df=0.85, min_df=0., max_features=n_features,
            stop_words=stop_words)
    tf = tf_vectorizer.fit_transform(text)
    model = LatentDirichletAllocation(n_topics=n_topics,
            max_iter=10,
            learning_method='online', learning_offset=50., random_state=0)
    model.fit(tf)

    tf_feature_names = tf_vectorizer.get_feature_names()

    tops = get_top_words(model, tf_feature_names, n_top_words)

    return tops


def get_top_words(model, feature_names, n_top_words):
    """! Gets top words from a fitted sklearn LDA model

    @param model (sklearn fitted LDA model)
    @param feature_names (list): list of strings, each string is the name
    of a feature
    @param n_top_words (int): number of top words on which to report from the
    model

    @return messages (list): a list of strings, each string is in the format
    'Topic <number>: word0, word1, ..., word<n_top_words>'
    """
    messages = []

    for topic_idx, topic in enumerate(model.components_):
	message = "Topic #%d: " % topic_idx
	message += " ".join([feature_names[i] for i in topic.argsort()[:-n_top_words
                    - 1:-1]])
	messages.append(message)

    return messages


def get_sentiment(text):
    """! Gets sentiment of words from text using VADER algorithm
    (see Hutto, Clayton J., and Eric Gilbert. "Vader: A parsimonious rule-based 
    model for sentiment analysis of social media text."
    In Eighth International AAAI Conference on Weblogs
    and Social Media. 2014.)

    @param text (string): text of which we want to find sentiment

    @return scores (list): sentiment associated with each word in text

    """
    getter = vader.SentimentIntensityAnalyzer()
    scores = [getter.polarity_scores(text[j])['compound'] for j in range(len(text))]
    return scores


def anes_data_preprocess(fname, test_size=0.1):
    """! Preprocesses data from the American National Election Studies
    (see http://www.electionstudies.org/)

    @param fname (string): filename of csv storing data in the ANES format
    @param test_size (float): size of data hold-out for testing, must be
    between zero and one (inclusive).

    @return text_dicts (dictionary): a dict of dicts. First level is
    text_dicts['train'] and text_dicts['test']. Keys in the second level are
    the header categories.
    """
    from sklearn.cross_validation import train_test_split
    text = csv2array(fname)
    header = text[0]
    sample_train, sample_test = train_test_split(text[1:], test_size=test_size)
    text_dicts = {}
    for sample in ['train', 'test']:
        text_dicts[sample] = {}
        j = 0

        for category in header:
            # each entry a person's response to category
            text_dicts[sample][category] = text[1:, j]
            j += 1

    return text_dicts


def words_report(text, n_features=1000, n_topics=4, n_top_words=4):
    """! Prints a report about topics from semantic_analysis.lda and
    semantic_analysis.get_sentiment on words

    @param text (iterable): an iterable of strings representing some text
    @param n_features (int): number of features for count vectorization,
    default is 1000.
    @param n_topics (int): number of topics, default is 4
    @param n_top_words (int): number of top words to report, default is 4
    """
    tops = lda(text, n_features, n_topics, n_top_words)
    for topic in tops:
        print('{}\n'.format(topic))
    scores = get_sentiment(text)  # big array of floats, should compute stats
    print('Average sentiment: {}\n'.format(np.mean(scores)))
    print('Std dev sentiment: {}\n'.format(np.std(scores)))


def find_grams(text, n=2, number=10, over=3):
    """! Find n-grams in text. Currently supports 2 and 3 grams, more coming.

    @param text (iterable): iterable of text in which we want to find ngrams
    @param n (int): <n>gram. Must be 2 or 3
    @param number (int): will return the top <number> ngrams. Default is 10.
    @param over (int): ngram must return >= <over> times to be counted. Default
    is 3.

    @return ngrams (list): list of tuples, each tuple is length <n> and
    contains an <n>gram.
    """
    assert n == 2 or n == 3
    assert (type(number) is int) and (number >= 1)

    # have to get rid of inapplicable again......
    new_text = [sentence for sentence in text if sentence != '-1 Inapplicable']
    just_words = []
    for sentence in new_text:
        just_words += sentence.replace('//',' ').replace(',', ' ').split()

    just_words = [word for word in just_words if (word != '')]
    s = " ".join(just_words)
    token = nltk.word_tokenize(s)

    if n == 2:
        measures = nltk.collocations.BigramAssocMeasures()
        finder = BigramCollocationFinder.from_words(token)
    else:
        measures = nltk.collocations.TrigramAssocMeasures()
        finder = TrigramCollocationFinder.from_words(token)

    finder.apply_freq_filter(over)  # only collect if appear 3 or more times
    ngrams = finder.nbest(measures.pmi, number)
    return ngrams


def ngrams_report(ngrams, n):
    """! Prints a report on ngrams found in text

    @param ngrams (list): a list of tuples, each tuple is an ngram. Should take
    output from semantic_analysis.find_grams
    @param n (int): the n of the ngrams
    """
    print('{} {}grams with highest PMI'.format(len(ngrams), n))
    for ngram in ngrams:
        print(" ".join(ngram)+'\n')

def report(text, key):
    """! Prints a report from LDA, sentiment analysis, and ngram finder
    Calls semantic_analysis.words_report and semantic_analysis.ngrams_report

    @param text (dict): text[key] are the words under the header <key>
    @param key (str): name of the words on which semantic_analysis.report is
    reporting
    """
    print('Report on {}\n'.format(key))
    words_report(text[key])
    twograms = find_grams(text[key], n=2)
    threegrams = find_grams(text[key], n=3)
    ngrams_report(twograms, 2)
    ngrams_report(threegrams, 3)


def comprehensive_report(text, list_of_keys):
    """! Calls semantic_analysis.report on each given key and prints output

    @param text (dict): text[key] are the words under the header <key>
    @param list_of_keys (list): list of strings, each string should be a
    category for which an analysis is desired
    """
    print('Possible categories: {}\n'.format(sorted(text.keys())))
    for key in list_of_keys:
        report(text, key)

if __name__ == "__main__":

    text_dicts = anes_data_preprocess('../data/anes2012TS_openends_PRE.csv')
    sample = text_dicts['train']
    comprehensive_report(sample, ['candlik_dislwhatdpc', 'candlik_dislwhatrpc',
        'candlik_likewhatdpc', 'candlik_likewhatrpc'])
