"""! rcsimple
David Dewhurst
Research and Development LLC, all rights reserved
DO NOT DISTRIBUTE
Copyright 2016-
"""


from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression, LogisticRegressionCV


def logistic_reg(data):
    """! Wrapper for sklearn's logistic regression. Returns coefficients,
    r value, and probabilities of class prediction

    @param data (2d array): first column should be dependent variable, rest of
    the columns should be design matrix

    @return coefs (iterable): list of regression coefficients
    @return score (float): coefficient of determination, -1 \leq r \leq 1
    @return probs (iterable): probability of data belonging to a class

    """

    y = []
    X = []

    for r in range(len(data)):
            y.append(data[r][0])
            X.append(data[r][1:])

    logit = LogisticRegressionCV()
    logit.fit(X, y)
    coefs = logit.coef_[0]
    score = logit.score(X, y)
    probs = logit.predict_proba(X)

    return coefs, score, probs


def linear_reg(data):
    """! Wrapper for sklearn linear regression. Returns coefficients, intercept,
    and r value.

    @param data (2d array): first column should be dependent variable, rest of
    the columns

    @return coefs (iterable): list of regression coefficients
    @return score (float): coefficient of determination, -1 \leq r \leq 1
    @return intercept (float): intercept of the calculated line
    """

    y = []
    X = []

    for r in range(len(data)):
            y.append(data[r][0])
            X.append(data[r][1:])

    lin = LinearRegression()
    lin.fit(X, y)
    coefs = lin.coef_
    score = lin.score(X, y)
    intercept = lin.intercept_

    return coefs, score, intercept
